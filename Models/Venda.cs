using System.Collections.Generic;

namespace tech_test_payment_api.Models
{
    public class Venda
    {

        public Venda()
        {
            this.DataVenda = DateTime.Now;
        }
        public DateTime DataVenda { get; set; }
        public int Id { get; set; }
        public string Status { get; set; }
        public int VendedorId { get; set; }
        public Vendedor Vendedor { get; set; }
        public int ProdutoId { get; set; }
        public Produto Produto { get; set; }

    }
}


