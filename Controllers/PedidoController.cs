using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;
using Microsoft.EntityFrameworkCore;


namespace tech_test_payment_api.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class PedidoController : ControllerBase
    {
        public PedidoContext _context { get; set; }

        public PedidoController(PedidoContext context)
        {
            this._context = context;
        }
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var retorno = _context.Vendas.Include(p => p.Produto)
                          .Include(v => v.Vendedor)
                          .Where(g => g.Id == id);
            return Ok(retorno.ToList());
        }


        [HttpPost]
        public IActionResult Criar(Venda venda)
        {
            _context.Vendas.Add(venda);
            _context.SaveChanges();

            return Ok(venda);
        }

        [HttpPut("{id}")]
        public IActionResult Atualizar(int id, Venda venda)
        {
            var vendaBanco = _context.Vendas.Find(id);
            if(vendaBanco.Status == "Aguardando Pagamento" && venda.Status == "Pagamento Aprovado")
            {
                vendaBanco.Status = venda.Status;
            }
            else if(vendaBanco.Status == "Aguardando Pagamento" && venda.Status == "Cancelada")
            {
                vendaBanco.Status = venda.Status;
            }
            else if(vendaBanco.Status == "Pagamento Aprovado" && venda.Status == "Enviado para Transportadora")
            {
                vendaBanco.Status = venda.Status;
            }
             else if(vendaBanco.Status == "Pagamento Aprovado" && venda.Status == "Cancelada")
            {
                vendaBanco.Status = venda.Status;
            }
             else if(vendaBanco.Status == "Enviado para Transportador" && venda.Status == "Entregue")
            {
                vendaBanco.Status = venda.Status;
            }
            _context.Vendas.Update(vendaBanco);
            _context.SaveChanges();
            return Ok(vendaBanco);


        }

        
    }
}