using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Models;
using System.Collections.Generic;

namespace tech_test_payment_api.Context
{
    public class PedidoContext : DbContext
    {
        public PedidoContext(DbContextOptions<PedidoContext> options) : base(options)
        {
            
        }
        public DbSet<Vendedor> Vendedores { get; set; }
        public DbSet<Venda> Vendas { get; set; }
        public DbSet<Produto> Produtos { get; set; }

        protected override void OnModelCreating (ModelBuilder builder)
        {
            builder.Entity<Vendedor>(tabela => 
            {
                tabela.HasKey(e => e.Id);

            });

             builder.Entity<Venda>(tabela =>
            {
                tabela.HasKey(e => e.Id);

            });

            builder.Entity<Produto>(tabela =>
               {
                   tabela.HasKey(e => e.Id);

               });
        }
    }
}